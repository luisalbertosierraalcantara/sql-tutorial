BACKUP DATABASE SQLTutorial_DB TO DISK = 'C:\MSSQLDB\SQLTutorial_DB.bak'
WITH DIFFERENTIAL,
NOFORMAT,
NOINIT,
NAME = 'SQLTutorial_DB Backup',
SKIP,
NOREWIND,
NOUNLOAD,
STATS = 10
