USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

-- DESABLE THE Foreign Key
ALTER TABLE Table_Customers
DROP CONSTRAINT FK_Table_Customers_Occupation

-- DELETE ALL RECORDS FROM the Table_Occupation table
DELETE FROM Table_Occupation

-- COPY ALL RECORDS TO Table_Occupation FROM Table_Occupation_temp
INSERT INTO Table_Occupation (occupation_id, name)
SELECT occupation_id, name FROM Table_Occupation_temp

-- ENABLE THE Foreign Key
ALTER TABLE Table_Customers
ADD CONSTRAINT FK_Table_Customers_Occupation 
FOREIGN KEY (occupation_id) REFERENCES Table_Occupation (occupation_id)

-- SHOW DATA
SELECT * FROM Table_Occupation