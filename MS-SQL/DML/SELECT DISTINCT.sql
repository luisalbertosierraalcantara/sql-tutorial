USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

-- Without Distinct
SELECT name, last_name, occupation_id, phone, email
FROM Table_Customers

-- With Distinct
SELECT Distinct name, last_name, occupation_id, phone, email
FROM Table_Customers