USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

MERGE INTO Table_Occupation  
USING Table_Occupation_temp
ON Table_Occupation.occupation_id = Table_Occupation_temp.occupation_id
WHEN MATCHED THEN
UPDATE SET 
Table_Occupation.name = Table_Occupation_temp.name
WHEN NOT MATCHED THEN
INSERT (occupation_id,name)
VALUES ((SELECT MAX(occupation_id)  + 1 FROM Table_Occupation),Table_Occupation_temp.name);


