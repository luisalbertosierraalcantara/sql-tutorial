USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA
GO 

CREATE VIEW Table_Customers_View
AS
SELECT 
Table_Customers.customers_id, 
Table_Customers.name, 
Table_Customers.last_name, 
Table_Customers.occupation_id, 
Table_Occupation.name as occupation,
Table_Customers.phone, 
Table_Customers.email
FROM Table_Customers 
INNER JOIN Table_Occupation ON Table_Occupation.occupation_id = Table_Customers.occupation_id

