USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

-- ALL
SELECT * FROM Table_Occupation WHERE occupation_id = ALL(SELECT occupation_id FROM Table_Occupation WHERE occupation_id = 3)
-- AND
SELECT * FROM Table_Occupation WHERE occupation_id = 3 AND name = 'Dentist'
-- ANY
SELECT * FROM Table_Occupation WHERE occupation_id = ANY (SELECT occupation_id FROM Table_Occupation WHERE occupation_id <> 3)
-- BETWEEN
SELECT * FROM Table_Occupation WHERE occupation_id BETWEEN 3 AND 6
--EXISTS
SELECT * FROM Table_Occupation WHERE EXISTS (SELECT occupation_id FROM Table_Occupation WHERE occupation_id = 6) AND occupation_id = 6
-- IN
SELECT * FROM Table_Occupation WHERE occupation_id IN (3,6)
-- LIKE
SELECT * FROM Table_Occupation WHERE name LIKE 'd%'
-- NOT
SELECT * FROM Table_Occupation WHERE  NOT occupation_id = 3 
-- OR
SELECT * FROM Table_Occupation WHERE occupation_id = 3 OR occupation_id = 6
-- SOME
SELECT * FROM Table_Occupation WHERE occupation_id < SOME (SELECT occupation_id FROM Table_Occupation WHERE occupation_id < 6)