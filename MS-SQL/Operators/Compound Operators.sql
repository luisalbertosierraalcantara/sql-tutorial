USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

-- +=
DECLARE @num INT = 4;  
SET @num += 2 ;  
SELECT @num AS Added_2; 
 
-- -=
DECLARE @num2 INT = 27;  
SET @num2 -= 2 ;  
SELECT @num2 AS Subtracted_2;
  
-- *=
DECLARE @num3 INT = 27;  
SET @num3 *= 2 ;  
SELECT @num3 AS Multiplied_by_2;
  
-- /=  
DECLARE @num4 INT = 27;  
SET @num4 /= 2 ;  
SELECT @num4 AS Divided_by_2; 
 
-- %= 
DECLARE @num5 INT = 27;  
SET @num5 %= 2 ;  
SELECT @num5 AS Modulo_of_27_divided_by_2;  

-- &= 
DECLARE @num6 INT = 9;  
SET @num6 &= 13 ;  
SELECT @num6 AS Bitwise_AND;
 
-- ^=  
DECLARE @num7 INT = 27;  
SET @num7 ^= 2 ;  
SELECT @num7 AS Bitwise_Exclusive_OR; 
 
-- |= 
DECLARE @num8 INT = 27;  
SET @num8 |= 2 ;  
SELECT @num8 AS Bitwise_OR;  