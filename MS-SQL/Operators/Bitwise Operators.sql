USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

-- &
SELECT * FROM Table_Occupation WHERE occupation_id < (2 & 3) 
-- |
SELECT * FROM Table_Occupation WHERE occupation_id = (4 | 5) 
-- ^
SELECT * FROM Table_Occupation WHERE occupation_id = (5 ^ 6) 
-- ~
SELECT * FROM Table_Occupation WHERE occupation_id > (~ 1)


