USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

-- =
SELECT * FROM Table_Occupation WHERE name = 'Doctor'
-- >
SELECT * FROM Table_Occupation WHERE occupation_id > 5
-- <
SELECT * FROM Table_Occupation WHERE occupation_id < 5
-- >=
SELECT * FROM Table_Occupation WHERE occupation_id >= 5
-- <=
SELECT * FROM Table_Occupation WHERE occupation_id <= 5
-- <>
SELECT * FROM Table_Occupation WHERE occupation_id <> 5