USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

-- Add
SELECT (5 + 5) AS _Add
-- Subtract
SELECT (5 - 5) AS _Subtract
-- Multiply
SELECT (5 * 5) AS _Multiply
-- Divide
SELECT (5 / 5) AS _Divide
-- Modulo
SELECT (5 % 5) AS _Modulo