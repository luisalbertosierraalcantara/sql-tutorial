USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

-- Table_Occupation
SELECT occupation_id, name from Table_Occupation

-- Table_Occupation_temp
SELECT occupation_id, name from Table_Occupation_temp

-- UNION OF BOTH
SELECT occupation_id, name from Table_Occupation
UNION
SELECT occupation_id, name from Table_Occupation_temp
