USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA
GO

CREATE FUNCTION SUM_VALUES(@VAL1 INT, @VAL2 INT)
RETURNS INTEGER
AS
BEGIN
DECLARE @RESULT INT;

SET @RESULT = @VAL1 + @VAL2;

RETURN @RESULT;
END;