USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA
GO 

CREATE PROCEDURE Table_Customers_insert_proc (
 @customers_id INT, 
 @name VARCHAR (20), 
 @last_name VARCHAR (50), 
 @occupation_id INT, 
 @phone VARCHAR (20), 
 @email VARCHAR (100))
AS
INSERT INTO Table_Customers
(customers_id, name, last_name, occupation_id, phone, email)
VALUES
(@customers_id, @name, @last_name, @occupation_id, @phone, @email)