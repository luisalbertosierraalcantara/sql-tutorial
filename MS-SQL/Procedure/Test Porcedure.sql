USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA
GO 

DECLARE @customers_id int
DECLARE @name varchar(20)
DECLARE @last_name varchar(50)
DECLARE @occupation_id int
DECLARE @phone varchar(20)
DECLARE @email varchar(100)

-- TODO: Set parameter values here.
SET @customers_id = 10
SET @name = 'Victor'
SET @last_name = 'Rojas'
SET @occupation_id = 6
SET @phone = '8290982222'
SET @email = 'vrojas@mail.com'

EXEC Table_Customers_insert_proc
   @customers_id
  ,@name
  ,@last_name
  ,@occupation_id
  ,@phone
  ,@email
GO

--EXEC Table_Customers_insert_proc 10,'Victor','Rojas',6,'8290982222','vrojas@mail.com'