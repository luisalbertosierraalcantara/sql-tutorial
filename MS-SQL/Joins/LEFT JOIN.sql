USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

SELECT 
TABLE1.customers_id, 
TABLE1.name, 
TABLE1.last_name, 
TABLE1.occupation_id, 
TABLE2.name,
TABLE1.phone, TABLE1.email
FROM Table_Customers AS TABLE1
LEFT JOIN Table_Occupation  AS TABLE2 ON TABLE1.occupation_id = TABLE2.occupation_id
