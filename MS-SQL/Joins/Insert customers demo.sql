USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

INSERT INTO Table_Customers(customers_id,name,last_name,occupation_id,phone,email)
VALUES (1,'Tito', 'Puentes',1,'8093343467','tpuentes@mail.com')

INSERT INTO Table_Customers(customers_id,name,last_name,occupation_id,phone,email)
VALUES (2,'Hector', 'Alvares',2,'8093340987','halvarez@mail.com')

INSERT INTO Table_Customers(customers_id,name,last_name,occupation_id,phone,email)
VALUES (3,'Josue', 'Martinez',3,'8093341211','jmatinez@mail.com')

INSERT INTO Table_Customers(customers_id,name,last_name,occupation_id,phone,email)
VALUES (4,'Ramon', 'Valdez',4,'8093341111','rvaldez@mail.com')

INSERT INTO Table_Customers(customers_id,name,last_name,occupation_id,phone,email)
VALUES (5,'Jairo', 'Matos',5,'8093345556','jmatos@mail.com')

INSERT INTO Table_Customers(customers_id,name,last_name,occupation_id,phone,email)
VALUES (6,'Luis A.', 'Sierra',6,'8093345455','lsierra@mail.com')

INSERT INTO Table_Customers(customers_id,name,last_name,occupation_id,phone,email)
VALUES (7,'Maria', 'Rosario',7,'8093346677','mrosario@mail.com')

INSERT INTO Table_Customers(customers_id,name,last_name,occupation_id,phone,email)
VALUES (8,'Manuela', 'Ramirez',8,'8093340099','mramirez@mail.com')

INSERT INTO Table_Customers(customers_id,name,last_name,occupation_id,phone,email)
VALUES (9,'Marta', 'Baez',9,'8093340000','mbaez@mail.com')