USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

SELECT 
TABLE2.customers_id, 
TABLE2.name, 
TABLE2.last_name, 
TABLE1.occupation_id, 
TABLE1.name,
TABLE2.phone, TABLE2.email
FROM Table_Occupation AS TABLE1
RIGHT JOIN Table_Customers  AS TABLE2 ON TABLE2.occupation_id = TABLE1.occupation_id
