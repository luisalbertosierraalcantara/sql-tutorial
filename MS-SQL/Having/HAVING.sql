USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

SELECT occupation_id,name
FROM Table_Customers
WHERE occupation_id < 8 
GROUP BY occupation_id,name
HAVING LEFT(name, 1) = 'M'
