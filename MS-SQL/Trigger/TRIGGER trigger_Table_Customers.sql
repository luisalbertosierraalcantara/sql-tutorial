USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA
GO
CREATE TRIGGER trigger_Table_Customers
ON Table_Customers
AFTER INSERT
AS
BEGIN
   DECLARE @FullName varchar(100)
   DECLARE @Last_Id_Inserted int
   SET @Last_Id_Inserted = (SELECT MAX(customers_id) from Table_Customers) 
   SET @FullName = (SELECT 'you inserted this name: ' + name + ' ' + last_name 
   FROM Table_Customers where customers_id = @Last_Id_Inserted)  
   PRINT @FullName
END


