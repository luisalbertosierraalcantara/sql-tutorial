USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

CREATE TABLE Table_Occupation_temp
(
	occupation_id int not null,
	name varchar(20) null
)

INSERT INTO Table_Occupation_temp (occupation_id, name)
VALUES (1,'Accountant');

INSERT INTO Table_Occupation_temp (occupation_id, name)
VALUES (2,'Mechanic');

INSERT INTO Table_Occupation_temp (occupation_id, name)
VALUES (3,'Dentist');

INSERT INTO Table_Occupation_temp (occupation_id, name)
VALUES (4,'Doctor');

INSERT INTO Table_Occupation_temp (occupation_id, name)
VALUES (5,'Carpenter');

INSERT INTO Table_Occupation_temp (occupation_id, name)
VALUES (6,'Engineer');

INSERT INTO Table_Occupation_temp (occupation_id, name)
VALUES (7,'Electrician')

INSERT INTO Table_Occupation_temp (occupation_id, name)
VALUES (8,'Chef');

INSERT INTO Table_Occupation_temp (occupation_id, name)
VALUES (9,'Farmer');


