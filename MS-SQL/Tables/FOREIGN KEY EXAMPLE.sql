USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

-- CREATE TABLE Table_Customers
CREATE TABLE Table_Customers 
(
	customers_id  int not null,
	name varchar(20) null,
	last_name varchar(50) null,
	occupation_id int not null,
	phone varchar(20) null,
	email varchar(100) null
)

-- ADDING PRIMARY KEY to Table_Customers
ALTER TABLE Table_Customers
ADD CONSTRAINT PK_Table_Customers PRIMARY KEY (customers_id);

-- CREATE TABLE Table_Customers
CREATE TABLE Table_Occupation
(
	occupation_id int not null,
	name varchar(20) null
)

-- ADDING PRIMARY KEY to Table_Occupation
ALTER TABLE Table_Occupation
ADD CONSTRAINT PK_Table_Occupation PRIMARY KEY (occupation_id);

-- ADDING FOREIGN KEY to Table_Customers 
ALTER TABLE Table_Customers
ADD CONSTRAINT FK_Table_Customers_Occupation
FOREIGN KEY (occupation_id) REFERENCES Table_Occupation (occupation_id);