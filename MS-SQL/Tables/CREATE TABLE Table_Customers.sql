USE SQLTutorial_DB

CREATE TABLE Table_Customers 
(
	id int null,
	name varchar(20) null,
	last_name varchar(50) null,
	occupation varchar(100) null,
	phone varchar(20) null,
	email varchar(100) null
)