USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

BEGIN  
	DECLARE @Full_Name varchar(200)

	SET @Full_Name = (SELECT TOP 1 name + ' ' + last_name from Table_Customers)

	SELECT @Full_Name as _Output
END      