USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

SELECT occupation_id, 
CASE
    WHEN occupation_id = 1 THEN 'Accountant'
	WHEN occupation_id = 2 THEN 'Mechanic'
	WHEN occupation_id = 3 THEN 'Dentist'
	WHEN occupation_id = 4 THEN 'Doctor'
	WHEN occupation_id = 5 THEN 'Carpenter'
	WHEN occupation_id = 6 THEN 'Engineer'
	WHEN occupation_id = 7 THEN 'Electrician'
	WHEN occupation_id = 8 THEN 'Chef'
	WHEN occupation_id = 9 THEN 'Farmer'
    ELSE 'None'
END AS occupation
FROM Table_Customers