USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

DECLARE @Full_Name varchar(200)
DECLARE @count int = 0

WHILE (@count < 9)
BEGIN  

	SET @Full_Name = (SELECT name + ' ' + last_name from Table_Customers where customers_id = @count)

	PRINT @Full_Name

	SET @count = @count + 1 

	IF @count = 9 
	   BREAK
	ELSE
	   CONTINUE
END      