USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

DECLARE @Full_Name AS nvarchar(100)
DECLARE Full_Name_Cursor CURSOR FOR SELECT name +' '+ last_name FROM Table_Customers
OPEN Full_Name_Cursor
FETCH NEXT FROM Full_Name_Cursor INTO @Full_Name

WHILE @@fetch_status = 0

BEGIN

    PRINT @Full_Name
    FETCH NEXT FROM Full_Name_Cursor INTO @Full_Name

END
CLOSE Full_Name_Cursor
DEALLOCATE Full_Name_Cursor
