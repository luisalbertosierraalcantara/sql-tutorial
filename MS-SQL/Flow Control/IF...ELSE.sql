USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA


DECLARE @occupation int
DECLARE @Full_Name varchar(200)

SET @occupation = (SELECT TOP 1 occupation_id from Table_Customers Where occupation_id = 1)
SET @Full_Name = (SELECT TOP 1 name + ' ' + last_name from Table_Customers Where occupation_id = 1)

BEGIN
	IF @occupation = 1 
	   SELECT @Full_Name + ' is an Accountant' as _Output
	ELSE
	   SELECT @Full_Name + ' is not Accountant' as _Output
END      