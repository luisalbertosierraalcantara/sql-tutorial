USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

--Format Number
SELECT FORMAT(8092345673, '###-###-####') AS phone

DECLARE @date DATETIME = '05/15/2024';
--Format Short Datetime
SELECT FORMAT (@date, 'd', 'en-US') AS 'US English Short Date'
--Format Long Datetime
SELECT FORMAT (@date, 'D', 'en-US') AS 'US English Long Date'