USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

declare @data table (prices decimal(18,2))

insert into @data
select 10.40
union 
select 14.60
union 
select 30.60
union 
select 40.80
union 
select 50.90
union 
select 60.09

SELECT MAX(prices) AS MaxPrice from @data 