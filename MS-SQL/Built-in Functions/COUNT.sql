USE SQLTutorial_DB
--https://interesting-homemade-projects.blogspot.com
--Author: LUIS A. SIERRA

declare @data table (id int)

insert into @data
select 1
union 
select 2
union 
select 3
union 
select 4
union 
select 5
union 
select 6

SELECT COUNT(id) AS ItemsQty from @data 